import sys

import boto3
import json

s3 = boto3.resource('s3')


def handler(event, context):
    try:
        print(f"Hello from AWS Lambda using Python {sys.version} {json.dumps(event)} !")
        event_id = str(event["event_id"])
        print(f"event id is {event_id}")
    except Exception as e:
        print(e)


if __name__ == "__main__":
    handler({"event_id": 1}, None)